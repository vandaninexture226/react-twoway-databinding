import { Component } from 'react';
import './App.css';
import Parent from './components/parentToChild/parent';

class App extends Component {
  state = {
    name: "vandan",
    title: "placeholder title"
  }

  changeName = (newName) => {
    this.setState({
      name: newName
    })
  }

  changeNameFrontInput = (event) => {
    this.setState({
      name: event.target.value
    })
  }

  changeTheWorld = (newTitle) => {
    this.setState({title:newTitle});
  }

  render() {
    return (
      <div className="App">
        <br /> <br />
        <button onClick={() => this.changeName('Awesome Vandan :(')}>Change state using Anon Function</button>
        <button onClick={this.changeName.bind(this,'Awesome Vandan :)')}>Change state using bind Function</button>
        <br /> <br />
        <input type="text" onChange={this.changeNameFrontInput} value={this.state.name} />
        <br />
        <div>{this.state.name}</div>
        <Parent changeTheWorldEvent={this.changeTheWorld.bind(this, 'new world')} 
          keepTheWorldEvent={this.changeTheWorld.bind(this, 'same world')}
        title={this.state.title} />
      </div>
    );
  }
}

export default App;
